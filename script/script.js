
// 1. Опишіть своїми словами що таке Document Object Model (DOM)
// DOM це сукупність елементів як об'єктів, які мають властивості та методи. Також є можливість для зміни стилю та структури
// документа напряму через JS.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerText надає можливість ставити або отримувати текстовий вміст елемента та його нащадків. innerHTML - встановлює чи
// отримує HTML або XML розмітку дочірніх елементів.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// querySelector, querySelectorAll, getElementById, getElementsByName, getElementsByTagName,
// getElementsByClassName. querySelector, querySelectorAll - найбільш популярні та універсальні селектори.


// 1
// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphs = document.querySelectorAll('p');

for (const iterator of paragraphs) {
    iterator.style.backgroundColor = '#ff0000';
}


// 2
// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const list = document.querySelector('#optionsList');
console.log(list);
console.log(list.parentNode);
const child = list.childNodes;
console.log(child);

if (child) {
    for (const iterator of child) {
        console.log('Назва: ' + iterator.nodeName + ' -> ', 'Тип: ' + iterator.nodeType);
    }
}

// 3
// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
const testParagraph = document.querySelector('#testParagraph');
testParagraph.textContent = 'This is a paragraph';
console.log(testParagraph);


// 4-5
// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const mainHeader = document.querySelector('.main-header');
const mainHeaderChildren = mainHeader.children;
console.log(mainHeaderChildren);
if (mainHeaderChildren) {
    for (const iterator of mainHeaderChildren) {
        iterator.classList.add('nav-item');
    }
}

// 6
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitle = document.querySelectorAll('.section-title');
if (sectionTitle) {
    for (const iterator of sectionTitle) {
        iterator.classList.remove('section-title');
    }
}








